public class Puppy1{
  int puppyAge;
  String puppyName;

  public Puppy1(int age, String name) {
    puppyAge = age;
    puppyName =name;
  }

  public static void main(String[] args) {
    Puppy1 myPuppy1 = new Puppy1(7, "Broody");
    System.out.println("Age:"+myPuppy1.puppyAge+ " "+"Name:"+myPuppy1.puppyName);
  }
}